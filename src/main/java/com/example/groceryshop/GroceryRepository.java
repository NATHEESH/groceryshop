package com.example.groceryshop;

import org.springframework.data.repository.CrudRepository;

public interface GroceryRepository extends CrudRepository<Product,Integer> {
}
