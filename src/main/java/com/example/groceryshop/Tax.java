package com.example.groceryshop;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Tax {
    @Id
    String typeOfProduct;
    int percent;

    public String getTypeOfProduct() {
        return typeOfProduct;
    }

    public void setTypeOfProduct(String typeOfProduct) {
        this.typeOfProduct = typeOfProduct;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    @Override
    public String toString() {
        return "Tax{" +
                "typeOfProduct='" + typeOfProduct + '\'' +
                ", percent=" + percent +
                '}';
    }
}
