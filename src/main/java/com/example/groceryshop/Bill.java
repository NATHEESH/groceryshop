package com.example.groceryshop;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Bill {

    //products.id, products.name, customer.quantity, products.price, taxAmount, (taxAmount + (customer.quantity * products.price))));
    @Id
    int id;
    String name;
    float quality;
    float unitPrice;
    float taxAmount;
    float price;
    public Bill()
    {

    }

    public Bill(int id, String name, float quality, float unitPrice, float taxAmount, float price) {
        this.id = id;
        this.name = name;
        this.quality = quality;
        this.unitPrice = unitPrice;
        this.taxAmount = taxAmount;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getQuality() {
        return quality;
    }

    public void setQuality(float quality) {
        this.quality = quality;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public float getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(float taxAmount) {
        this.taxAmount = taxAmount;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Bill{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", quality=" + quality +
                ", unitPrice=" + unitPrice +
                ", taxAmount=" + taxAmount +
                ", price=" + price +
                '}';
    }
}
