package com.example.groceryshop;

import org.springframework.data.repository.CrudRepository;

public interface TaxRepository extends CrudRepository<Tax,String> {
}
