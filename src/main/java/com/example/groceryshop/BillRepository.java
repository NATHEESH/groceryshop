package com.example.groceryshop;

import org.springframework.data.repository.CrudRepository;

public interface BillRepository extends CrudRepository<Bill,Integer> {
}
