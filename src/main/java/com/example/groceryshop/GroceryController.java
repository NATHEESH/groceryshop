package com.example.groceryshop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class GroceryController {
    @Autowired
    public GroceryRepository groceryRepository;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    TaxRepository taxRepository;
    @Autowired
    BillRepository billRepository;

    @RequestMapping("/")
    public String home() {
        return "home.html";
    }

    @RequestMapping("/addProduct")
    public String addProduct(Product product) {
        groceryRepository.save(product);
        return "home";
    }

    @RequestMapping("/getProduct")
    public String getProduct(@RequestParam int id, Model model) {
        Product products = groceryRepository.findById(id).orElse(new Product());
        model.addAttribute("specifictag", products.toString());
        return "home";

    }

    @RequestMapping("/listallproduct")
    @ResponseBody
    public String getAllProducts() {
        return groceryRepository.findAll().toString();
    }

    @RequestMapping("/listspecificproduct/{id}")
    @ResponseBody
    public String getSpecificProducts(@PathVariable("id") int id) {
        return groceryRepository.findById(id).toString();
    }

    /*********************/
    @RequestMapping("/tax")
    public String taxHome() {
        return "tax.html";
    }

    @RequestMapping("/addTax")
    public String addProduct(Tax tax) {
        taxRepository.save(tax);
        return "tax.html";
    }

    /************************/
    @RequestMapping("/customer")
    public String customerHome() {
        return "customer.html";
    }

    @RequestMapping("/purchasedItem")
    public String purchasedItem(Customer customer) {
        Product products = groceryRepository.findById(customer.id).orElse(new Product());
        Tax tax = taxRepository.findById(products.type).orElse(new Tax());
        float taxAmount = (tax.percent / 100f) * products.price;
        billRepository.save(new Bill(products.id, products.name, customer.quantity, products.price, taxAmount, (taxAmount + (customer.quantity * products.price))));

        return "customer.html";
    }

    @RequestMapping("/billTheProduct")
    @ResponseBody
    public String billTheProduct() {
        return billRepository.findAll().toString();
    }

    @RequestMapping("/checkOut")
    public String findAll(Model model) {

        String result = "<br>";
        float totalAmountToPay = 0;
        float discountPercentage = 0;
        float discoutPrice = 0;
        float totalAmountToPayWithDiscount = 0;
        for (Bill bill : billRepository.findAll()) {
            totalAmountToPay += bill.price;
            result += bill.toString() + "<br>";
        }
        result += "Total Bill" + totalAmountToPay + "<br>";
        if (totalAmountToPay >= 2000f) {
            discountPercentage = 5;
        } else if (totalAmountToPay >= 1000f) {
            discountPercentage = 2;
        }
        discoutPrice = (discountPercentage / 100f) * totalAmountToPay;

        result += "<br>" + "Discount (" + discountPercentage + ")" + discoutPrice + "<br>";
        result += "<br>" + "Total with discount" + totalAmountToPayWithDiscount + "<br>";

        model.addAttribute("tagtwo", result);
        return "customer.html";
    }
}

