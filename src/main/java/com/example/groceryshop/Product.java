package com.example.groceryshop;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class Product {

    @Id
    int id;
    String name;
    String type;
    float price;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", price=" + price +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
